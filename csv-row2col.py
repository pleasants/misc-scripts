#!/usr/bin/env python
import re, sys

try:
	dids = sys.argv[1]
	list_did = re.split(', ',dids)
	didfile = open('column_out.csv', 'a')

	for i in list_did:
		didfile.write(i+'\n')
	didfile.close()
except:
	print("Usage: ", sys.argv[0], "'val1, val2, val3,...'")
	print("This will output a file titled 'column_out.csv'")
	print("Please use single quotes around list items.")