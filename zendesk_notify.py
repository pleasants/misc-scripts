#!/usr/bin/env python

import requests, json, sys, getpass, csv, argparse

parser = argparse.ArgumentParser(description='Send notifications to customers')
parser.add_argument('file',  help='CSV file in format EMAIL,DID[,DID]...')

##global vars
arg = parser.parse_args()
user = ''
pswd = ''

url = 'https://company.zendesk.com/api/v2/tickets.json'
headers = {'content-type': 'application/json'}

subject = '[Email Subject] '
body1 = 'Email content'
body2 = 'More email content'


url = 'https://company.zendesk.com/api/v2/tickets.json'
headers = {'content-type': 'application/json'}

def login():
    global user
    global pswd
    user = input('username: ')
    pswd = getpass.getpass('password: ')

def csvparse():
    data = []
    with open(arg.file, 'r') as f:
        reader = csv.reader(f)
        for row in reader:
            print(row)
            data.append({'email': row[0], 'dids': row[1::]})
    return data

def sendemail(data):
    for element in data:
        if len(element['dids']) <= 2:
            sub= subject + " & ".join(element['dids'])
        else:
            sub= subject + "Multiple TNs"
        ticket = {'ticket':{'requestor':{'email':element['email']}, 'subject':sub, 'comment':{'body':body1 + '\n'.join(element['dids']) + body2}}}
        payload = json.dumps(ticket)
        print(payload)
        response = requests.post(url, data=payload, auth=(user, pswd), headers=headers)

        #Check response
        if response.status_code != 201:
            print("Status: ", response.status_code, 'Error!')
            #exit()

        #Report success
        print('Sucessfully created ticket.')
        print(response.json())
        #print ticket number

def main():
    if user == '' or pswd == '' :
        login()
    data = csvparse()
    sendemail(data)
    exit() 

if __name__ == "__main__":
    main()
