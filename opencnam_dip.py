import sys
import urllib.request
import socket
import credentials

did = sys.argv[1] if len(sys.argv[1]) == 10 else sys.argv[1][1:]
sid = credentials.open_cnam_sid
token = credentials.open_cnam_token

request = (f'https: // api.opencnam.com/v3/phone/+1{did}?account_sid = {sid} '
           '& auth_token = {token}')

try:
    result = urllib.request.urlopen(request)
except Exception as e:
    # if opencnam dip fails, cnam = did.
    print("1", sys.argv[1] if len(sys.argv[1]) == 10 else sys.argv[1])
else:
    print(result.read())
