#!/bin/bash
# New user setup script. OSX 
scripts="$HOME/scripts"

path_updater() {
  if [[ -s "$HOME/.bash_profile" ]]; then
    if grep "PATH=" "$HOME/.bash_profile" &>/dev/null; then
      if grep "$1:" "$HOME/.bash_profile" &>/dev/null; then
        echo -e "\nPATH is already set correctly."
        return
      else
        sed -i -e 's;^PATH=.*;PATH='"$1"':'"$PATH"';' "$HOME/.bash_profile"
      fi
    else
      echo -e "$1:$PATH\nexport PATH" >> "$HOME/.bash_profile"
    fi
  else
    echo -e "PATH=$1:$PATH\nexport PATH" >> "$HOME/.bash_profile"
  fi
  source "$HOME/.bash_profile"
}

# Script Begins
cd || { echo "Failed to navigate to the home directory. Exiting"; exit 1; }
echo -e "\nWelcome to the LNP/Support setup script.\nYou can quit at any time by pressing control+c.\n"

# Brew Installer
# Needs xcode?
while true; do
  read -r -p "Install Brew? (Y)es/(N)o/(Q)uit: " yn1
  case $yn1 in
    [Yy]*)
	    echo
      ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
      break
      ;;
    [Nn]*)
      break
      ;;
    [Qq]*)
      echo "Exiting..."
      exit
      ;;
    *)
      echo "Please enter a valid response."
      ;;
  esac
done

# Python Installer
while true; do 
  read -r -p "Install Python? (Y)es/(N)o/(Q)uit: " yn2
  case $yn2 in
    [Yy]*)
	    echo -e  "Installing Python and setting PATH in .bash_profile."
	    brew install python
	    brew unlink python
	    brew link python
	    brew linkapps
      path_updater "/usr/local/bin:/usr/local/sbin"
      break
      ;;
    [Nn]*)
      break
      ;;
    [Qq]*)
      echo "Exiting..."
      exit
      ;;
    *)
      echo "Please enter a valid response."
      ;;
  esac
done

#Script Installer
while true; do
  read -r -p "Install support and lnp tools? (Y)es/(N)o/(Q)uit: " yn3
  case $yn3 in
    [Yy]*)
      read -n 1 -r -p "Please connect to the VPN, sign in to http://gitlab using your
LDAP credentials, then return here and press any key to continue."
      if [[ -d "$scripts/path1" || -d "$scripts/path2" || -d "$scripts/path3" ]]; then
        while true; do
          read -r -p "It looks like you already have scripts installed. Do you wish to backup and 
reinstall ops, lnp, and api scripts? (Y)es,(N)o,(Q)uit: " yn4
          case $yn4 in
            [Yy]*)
              mkdir "$scripts/bak"
              mv "$scripts/path1" "$scripts/path2" "$scripts/path3" "$scripts/bak"
              echo "Reinstalling sngrep, ops, lnp, and API related scripts..."
              git clone http://repo "$scripts/path1"
              git clone http://repo "$scripts/path2"
              git clone http://repo "$scripts/path3"
              brew install sngrep
              break
              ;;
            [Nn]*)
              break
              ;;
            [Qq]*)
              echo "Exiting..."
              exit
              ;;
            *)
              echo "Please enter a valid response."
              ;;
          esac
        done
      else 
        mkdir "$scripts" "$scripts/path1" "$scripts/path2" "$scripts/path3" "$scripts/sngrep"
        echo "Installing sngrep, ops, lnp, and API related scripts..."
        git clone --depth=1 http://repo "$scripts/path1"
        git clone --depth=1 http://repo "$scripts/path2"
        git clone --depth=1 http://repo "$scripts/path3"
        brew install sngrep
        pip install -r "$scripts/path1/requirements.txt"
        pip install -r "$scripts/path2/requirements.txt"
        pip install -r "$scripts/path3/requirements.txt"
        # if [[ ! -s $HOME/.sup_aliases ]]; then          
        #   awk -F/ '{"grealpath " $0|getline rpath; split($NF,a,"."); print "alias " a[1] "=" rpath; close("grealpath " $0)}' <<< "$(find $scripts/path1 -maxdepth 1 -perm -111 -type f)" >> $HOME/.sup_aliases 
        # fi
      fi
      echo -e "\nAdding scripts PATHs to BASH profile. Should you be running an alternative \nshell environment, please add these four repos to your PATHs variable in \nyour environments equivalent of .profile. You can find which this is by googling\nyour environment name followed by .profile i.e. zsh .profile."
      path_updater "$scripts/path1:$scripts/path2:$scripts/path3"
      break
      ;;
    [Nn]*)
	    break
      ;;
    [Qq]*)
      echo "Exiting..."
      exit
      ;;
    *)
      echo "Please enter a valid response."
      ;;
  esac
done

echo "Exiting."
exit
