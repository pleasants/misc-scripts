import requests
import json
import pprint
import time
import credentials
from openpyxl import Workbook
from openpyxl import  load_workbook

headers = {'content-type': 'application/json'}
url = credentials.zipwhip_url
api_key = credentials.zipwhip_api_key

wb = load_workbook('export.xlsx') # load the example XLS file
ws = wb.get_active_sheet() # set current sheet as active sheet

tn = input("Enter 11-digit, space-separated TN(s) you'd like to lookup: ")
print()
list_tns = tn.split()

n = 2

for i in list_tns:
    params = {'api_key': api_key, 'phone_number': i.split()}
    response = requests.get(url, params=params, headers=headers)
    print(response.status_code)
    pprint.pprint(response.text)
    response = json.loads(response.text)

    try:
        status = response['status_desc']
        status_code = response['status_code']
        status_description = response['status_desc']
        account_status = response['account_status']
    except KeyError:
        pass

    try:
        ws['A{n}'] = i
        ws['B{n}'] = status
        if status_code == "408":
            ws['B{n}'] = status_description
        ws['C{n}'] = account_status
    except NameError:
        pass

    n += 1

    time.sleep(1)
    
print("Complete!")
wb.save("export_complete.xlsx")