## cnam-lookup

looks up caller-id name via privledged server. 

---

## csv-row2col.py

Turns x, y, z into 

x

y

z

---

## opencnam_dip.py

looks up cnam record using the opencnam API.

---

## pcap_helper

securely runs filtered packet captures either live or in the background on several remote servers.

---

## pcap_helper_tui.sh

generates valid pcap_helper commands based on a series of questions.

---

## support_env_setup.sh

Set up an OSX environment for new support employees.

---

## support_scorecard.py

Pulls important metrics from phone and ticketing system exports.

---

## toolbox.sh

"dumb" script that pulls a configurable set of tools over http. noisy.

---

## zendesk_notify.py

Generates a ZenDesk ticket with an outbound email.

---

## zipwhip_api_check.py

checks the provisioning status of a phone number against the zipwhip API. Requires key and endpoint. 
