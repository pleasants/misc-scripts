#!/bin/bash
# Simple script for installing local copies of scripts located on a remote server.
# The original intention was to give support reps a TUI frontend for several support related tools.

name="Toolbox Script"
server="http://remote.server/tools"

#####################################################################
firstrun ()
{
echo "
  Installing script system...
  Making scripts directory...
"
mkdir .scripts 2>> /dev/null

cd .scripts || exit
wget -N $server/toolbox &> /dev/null || { echo "Error getting $name from server." ; exit; }
cd || exit
chmod a+x .scripts/toolbox 2> /dev/null
 if ! grep ".scripts" .bash_profile | grep -vi "Alias" ; then
  echo -e "PATH=$PATH:$HOME/.scripts\nexport PATH" >> .bash_profile
  echo -e "\E[34m Toolbox and script system installed.\n To initialize, type: \E[37msource .bash_profile\E[34m\n The toolbox can then be run with: \E[37mtoolbox"
else
  echo -e "\E[34m Toolbox and script system installed.\n The toolbox can be run with: \E[37mtoolbox"
fi
}

#####################################################################
# Enter the home directory
cd || exit

# Checks server for toolbox related alerts.
if alert=$(curl -s $server/alert.msg 2> /dev/null) ; then
 echo -e "$alert"
fi

# Search for the scripts directory. If not present, run through install.
if ! [ -d .scripts ]; then
  firstrun
fi

# Install and run scripts listed at $server/options.list
options=$(curl -s $server/options.list)
IFS=$'\n'
export IFS
echo -e " Select a script to install\n"
PS3="Option:"
select option in $options Exit
do
  case $option+$REPLY in
    Exit+*)
      echo "Exiting"
      exit 0
      ;;
  # Valid input range limited to number of entries in options.list
    *+[1-$(wc -l <<< "$options")])
      file=$(curl -s "$server"/array.list | grep "$option" | awk -F: '{ print $NF }')
      wget -N "$server"/"$file" -O "$HOME"/.scripts/"$file" &> /dev/null || { echo "Error getting $file from server." ; exit; }
      chmod a+x .scripts/"$file"
      # Tools within the toolbox will display the alert.msg if the child flag is not set.
      child=1
      exec .scripts/"$file"
      ;;
  esac
done
