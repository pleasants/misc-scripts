import os
import csv
import argparse
from dateutil.parser import parse


parser = argparse.ArgumentParser(description="Pull support stats from exported contivio & desk CSV. Last Updated: 08/24/2017\n" \
        "ex: $ python support_scorecard.py -t 2017-08-24_desk.csv -p ~/scripts/2017-08-24_contivio.csv -s 2017-08-14 -e 2017-08-20")
parser.add_argument('-t', dest='TICKET', \
    help='Path of Desk ticket statistics CSV export (ex: -t 2017-08-24_desk.csv)')
parser.add_argument('-p', dest='PHONE', \
    help='Path of Contivio phone statistics CSV export (ex: -p 2017-08-24_contivio.csv)')
parser.add_argument('-s', dest='START', \
    help='Start Date of stats filter (Monday). (ex: -s 2017-08-14)')
parser.add_argument('-e', dest='END', \
    help='End Date of stats filter (Sunday). (ex: -e 2017-08-20)')

args = parser.parse_args()


def format_date(str_date):
    date = parse(str_date)
    return date.strftime('%Y-%m-%d')


def get_csat_total(rating):
    ratings = {1: 33.33, 2: 66.67, 3: 100.0}
    return ratings[rating]


def parse_desk_csv(file_name, start_date, end_date):
    '''Pulls relevant ticket data from Desk export'''
    csat_cnt = 0
    csat_total = 0
    tickets_vendor = 0
    tickets_outage = 0
    tickets_created = 0
    tickets_resolved = 0

    with open(file_name) as csv_file:
        desk_records = csv.DictReader(csv_file)
        # get created and resolved tickets from date range
        for row in desk_records:
            # long conditionals
            support = row['Assigned Group'] == 'Support'
            resolved = row['Resolved At'] != ''
            spam = 'Spam' not in row['Case Custom Field 26 (Root Cause)']
            is_outage = 'outage' in row['Labels']
            is_vendor = 'VENDOR_TICKET' in row['Labels']

            if support and resolved and not spam:
                resolved_date = format_date(row['Resolved At'])

                if resolved_date >= start_date and resolved_date <= end_date:
                    tickets_resolved += 1

                    if row['CSAT Rating']:
                        csat_total = get_csat_total(row['CSAT Rating'])
                        csat_cnt += 1
            
            elif support and not spam:
                created_date = format_date(row['Created At'])
                resolved_date = format_date(row['Resolved At'])
                
                if created_date >= start_date and resolved_date <= end_date:
                    tickets_created += 1
                tickets_vendor += 1 if is_vendor else 0
                tickets_outage += 1 if is_outage else 0

        csat_average = round(csat_total / float(csat_cnt), 2)
        csat_return_rate = round((csat_cnt * 100.00) / tickets_resolved, 2)

    return {'average': csat_average,
            'return_rate': csat_return_rate,
            'created_count': tickets_created, 
            'resolved_count': tickets_resolved,
            'vendor_count': tickets_vendor,
            'outage_count': tickets_outage,
            }


def valid_call(row):
    '''checks of a call matches the desired filter'''
    afterhours = 'AfterHours' in row['To']
    unique = row['SameCall'] == 'FALSE'
    ivr_abandoned = row['Call Result'] == 'IVR_ABANDONED'
    inbound = 'INBOUND' in row['Media Type']
    voicemail = 'VOICEMAIL' in row['Call Result']
    queues = ['Queue 1', 'Queue 2', 'Queue 3']

    if (not afterhours and not ivr_abandoned and not voicemail \
        and inbound and unique and row['Queue/Campaign'] in queues):
        return True
    else:
        return False



def parse_call_csv(file_name, start_date, end_date):
    '''Pulls relevant call data from Contivio export'''
    queue_cnt =	0
    queue_success_cnt  = 0
    avg_abandon_sec = 0
    outbound_cnt = 0
    outbound_talk_sec = 0
    queue_abandon_cnt = 0
    queue_answer_sec = 0
    avg_team_call_min = 0
    queue_talk_sec = 0
    # list of call takers
    agents = ['Agent 1', 'Agent 2', 'Agent 3', 'Agent 4',
              'Agent 5', 'Agent 6', 'Agent 7', 'Agent 8', 'Agent 9']

    # format the CSV file in working directory & iterate rows
    with open(file_name) as csv_file:
        call_records = csv.DictReader(csv_file)
        # get relevant data within CSV
        for row in call_records:
            # format call date
            call_date = format_date(row['Time of Call'])
            # more long conditionals
            outbound = 'OUTBOUND' in row['Media Type']
            abandoned = row['Call Result'] == 'QUEUE_ABANDONED'
            cancelled = row['Call Result'] == 'CANCELLED'
            call_in_range = call_date >= start_date and call_date <= end_date
            # get relevant business hour calls
            if call_in_range and valid_call(row):
                queue_cnt += 1
                # get Queue Abandons & AVG Abandon (sec)
                if abandoned or cancelled:
                    queue_abandon_cnt += 1
                    avg_abandon_sec += int(row['Queue Duration'])
                # get Successful Calls, AVG Team call (sec) & AVG Answer speed (sec)
                if row['Call Result'] == 'SUCCESS' or row['Call Result'] == 'CALLBACK':
                    queue_success_cnt += 1
                    queue_talk_sec += int(row['Call Duration'])
                    # get Total queue duration (sec)
                    if not outbound:
                        queue_answer_sec += int(row['Queue Duration'])
            # get Outbound count & Outbound Call (sec)
            elif outbound and int(row['Talk Time']) > 30  and row['Agent'] in agents:
                outbound_cnt += 1
                outbound_talk_sec += int(row['Call Duration'])

        # get Total calls & Call time
        total_calls_cnt = queue_cnt + outbound_cnt
        total_calls_sec = queue_talk_sec + outbound_talk_sec
        # get AVG Team call (min)
        avg_team_call_min = round((total_calls_sec / total_calls_cnt) / 60.0, 2)
        # get AVG Abandon speed (min)
        avg_abandon_min = round((avg_abandon_sec / queue_abandon_cnt) / 60.0, 2)
        # get AVG Answer speed (min)
        avg_answer_min = round((queue_answer_sec / queue_success_cnt) / 60.0, 2)

    return {
    'calls_total': total_calls_cnt,
    'calls_outbound': outbound_cnt, 
    'calls_successful': queue_success_cnt, 
    'calls_abandoned': queue_abandon_cnt, 
    'calls_abandon_avg': avg_abandon_min,
    'calls_success_avg': avg_answer_min,
    'calls_team_avg': avg_team_call_min
    }


def print_csat_data(data):
    print('CSAT numbers:', data['average'])
    print('Return rates (%):', data['return_rate'])


def print_ticket_data(data):
    print('Tickets Created:', data['created_count'])
    print('Tickets Resolved:', data['resolved_count'])
    print('Vendor Tickets Created:', data['vendor_count'])
    print('Outage Tickets Created:', data['outage_count'])


def print_call_data(data):
    print('Total Calls:', data['calls_total'])
    print('Total Outbound:', data['calls_outbound'])
    print('Successful Calls:', data['calls_successful'])
    print('Queue Abandons:', data['calls_abandoned'])
    print('AVG Abandon (min):', data['calls_abandoned_avg'])
    print('AVG Answer speed (min):', data['calls_success_avg'])
    print('AVG team call (min):', data['calls_team_avg'])


def main():
    ticket_stats = args.TICKET
    call_stats = args.PHONE
    start_date = args.START
    end_date = args.END

    desk_data = parse_desk_csv(ticket_stats, start_date, end_date)
    call_data = parse_call_csv(call_stats, start_date, end_date)

    print_csat_data(desk_data)
    print('---')
    print_call_data(call_data)
    print('---')
    print_ticket_data(desk_data)


if __name__ == "__main__":
    main()

#TODO write directly to 'data' sheet
