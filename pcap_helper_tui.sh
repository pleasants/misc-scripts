#!/bin/bash

# creates a valid command for the pcap_helper utility and sends it to the clipboard.
END=$(date -u "+%Y-%m-%d% %H:%M:%S")
START=$(date -v-1H -u "+%Y-%m-%d% %H:%M:%S")
TIME=""

function usage {
	echo "Usage pcap_helper OPTIONS

		Options:
		-o  Server 1

		-i  Server 2

		-p  Server 3

		-l Live mode"
}

while getopts "oiplh" OPTION
do
case $OPTION in

    o)
    echo "Pattern to search?"
    read -e PATTERN
    echo "Time (in minutes)"
    read -e TIME
    if [ -z "$TIME" ]
        then
            echo -n "pcap_helper -s server1 -c '$PATTERN' -i '$START' -e '$END'" | pbcopy;
            echo "Your command is ready in your clipboard. Have a nice day";
        else
            CSTART=$(date -v-"$TIME"M -u "+%Y-%m-%d% %H:%M:%S");
            echo -n "pcap_helper -s server1 -c '$PATTERN' -i '$CSTART' -e '$END'" | pbcopy;
            echo "Your command is ready in your clipboard. Have a nice day";
        fi
    ;;
    i)
    echo "Pattern to search?"
    read -e PATTERN
    echo "Time (in minutes)"
    read -e TIME
    if [ -z "$TIME" ]
        then
            echo -n "pcap_helper -s server2 -c '$PATTERN' -i '$START' -e '$END'" | pbcopy;
            echo "Your command is ready in your clipboard. Have a nice day";
        else
            CSTART=$(date -v-"$TIME"M -u "+%Y-%m-%d% %H:%M:%S");
            echo -n "pcap_helper -s server2 -c '$PATTERN' -i '$CSTART' -e '$END'" | pbcopy;
            echo "Your command is ready in your clipboard. Have a nice day";
        fi
    ;;
    p)
    echo "Pattern to search?"
    read -e PATTERN
    echo "Time (in minutes)"
    read -e TIME
    if [ -z "$TIME" ]
        then
            echo -n "pcap_helper -s server3 -c '$PATTERN' -i '$START' -e '$END'" | pbcopy;
            echo "Your command is ready in your clipboard. Have a nice day";
        else
            CSTART=$(date -v-"$TIME"M -u "+%Y-%m-%d% %H:%M:%S");
            echo -n "pcap_helper -s server3 -c '$PATTERN' -i '$CSTART' -e '$END'" | pbcopy;
            echo "Your command is ready in your clipboard. Have a nice day";
        fi
    ;;
    l)
    echo "Pattern to search?"
    read -e PATTERN
    echo "Server? server1 or server2 or server3"
    read -e SERVER
    if [ "$SERVER" == server1 ]
        then
            echo -n "pcap_helper -s $SERVER.address -c '$PATTERN'" | pbcopy;
        else
            echo -n "pcap_helper -s $SERVER.different.address -c '$PATTERN'" | pbcopy;
        fi
    echo "Your command is ready in your clipboard. Have a nice day";
    ;;
    h*)
    usage
    exit 1
    ;;
esac
done
